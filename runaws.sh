#!/bin/bash
echo "validation is dev"
echo "running 3 layer linear Train:62.75 Dev:62.53 Test:62.69"
python testv2-aws-3layerlinear.py > aws-linear
echo "T7600 Train:88.99 Dev:79.39 Test:78.73"
python testv2-aws-3layernonlinear.py > aws-nonlinear
echo "T7600 Train:85.54 Dev:78.7 Test:78.13"
python testv2-aws-3layernonlineartanh.py > aws-nonlineartanh
echo "4096 T7600 Train:90.16 Dev:79.18 Test:78.92"
python testv2-aws-3layernonlinear4096.py > aws-4096
echo "dropout122 not 4096 all dropout is min 2 layers Train:83.27 Dev:78.95 Test:78.83 "
python testv2-aws-3layernonlinearDropout122.py > aws-Dropout122
echo "dropout 122 4 num_layers Train:71.2 Dev:41.3 Test:41.17"
python testv2-aws-3layernonlinearDropout122-4layers.py > Dropout122-4numlayers
echo "dropout 122 4096 Adam 3 num_layers Train: Dev: Test:"
python testv2-aws-3layernonlinearDropout122-3layers.py > Dropout122-3numlayers
echo "droupout125 4096 Adam 2 num_layers Train: Dev: Test:"
python testv2-aws-3layernonlinearDropout125.py > Dropout125
echo "dropout155 4096 adam 3 num_layers Train: Dev: Test:"
python testv2-aws-3layernonlinearDropout125-3layers.py > Dropout125-3numlayers
echo "dropout 155 4096 Adam 2 num_layers Train: Dev: Test:"
python testv2-aws-3layernonlinearDropout155.py > Dropout155
echo "dropout 155 4096 Adam 2 num_layers Train: Dev: Test:"
python testv2-aws-3layernonlinearDropout155-3layers.py > Dropout155-3numlayers
echo "adam Train:96.04 Dev:80.25 Test:79.75"
python testv2-aws-3layernonlinearAdam.py  > aws-adam
